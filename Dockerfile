FROM faucet/python3

CMD ['mkdir' , 'order-book']
COPY . ./order-book

CMD ['pip', 'install', '-r', 'requirements.txt']
CMD ['python3', 'main.py']
CMD ['']