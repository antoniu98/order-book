# order-book

  
  

## Introduction

  

Hello, my name is Antoniu, 23, Software Engineer from Romania. 

## General flow and architecture

I aimed to create many independent components which can be further developed in microservices.

Following OOP principles, I kept every class down to a very specific task, kept the interaction between components through parameter injection and used design patterns such as Command, Factory, Singleton (logger) and Observerer.

The program flow starts with reading on a given CSV file and converts each record line into an Order object which, as I figured out from the CSV files, has a different message format depending on the transaction types (I called it OrderTypes).  

Each Order is passed to the **OrderPublisher**. This entity is responsible for receiving the orders after they were read and distribute them to the "readers" or "processors". For each symbol there is a dedicated processor (ex: IBM Processor) which handles the business logic for the interaction between orders and order book. I've chosen the Observerer / Publish-Subscribe pattern (see also the Improvements section), as each processor is interested in 'messages' containing only its symbol. Thus, the OrderPublisher keeps a dictionary with symbols and associated processor and will notify each one of them depending on the received order. When a new symbol arrives, a new processor is created using the **ProcessorFactory** (Factory pattern).

The **ProcessorSymbol** is the entity which keeps track of the OrderBook of a symbol. It receives messages from the OrderPublisher, processes the order and changes the order book accordingly. I chose the implement the OrderBook class inside the ProcessorSymbol as an ordered book is associated with a symbol and cannot exist without it. 

The **OrderBook** keeps track of Selling/Asking Offers and Buying/Bidding Offers. Since the order of the 2 lists is different, I chose to have 2 different heaps to store them, as the comparing method will differ (see Notes in class definition about maxheap in python implementation).
For the storing data structure I chose the **Heap**  (min and max). We will check very often the lowest 'ask' and highest 'bid' (which can be done in O(1) ), we will add new orders/offers to the heap ( O(logN) ) and we will extract best offers as new orders arrive ( O(logN) ).

Depending on the order, we have to execute different steps. Thus, I considered it is the best to use the Command pattern, where every different order/transaction type has its own command (see Improvements). Each command will execute its set of steps on the given parameters. The operations performed on the OrderBook are through the API of the ProcessorSymbol.
  
* Dockerfile also implemented 
>cd <path/to/dockerfile>
>docker build -t order-book .

## Limitations of the implementation

  

Due to time concerns, I kept the functionality as close to the requirements as possible, excluding though the following:

- *Limited tests*: The unit test was implemented once with the OrderBook logic. The integration test describes a basic scenario ran against the program. The CSV records are being converted into Order objects. As time was limited, I didn't pay the most attention to the tests, as the only test ca
- Only the N transaction type orders are being processed, with easy implementation of other types due to decoupled components.
- Implemented only the new order acknowledgement

  
  

## Chalenges

- The biggest challenge to be honest was to understand the definition of the linked terms from the task and how the  inputs and outputs records work. It took a while for me to understand the flow (didn't quite catch some parts) and how orders affect the order book (thanks google and youtube).
- Architecture: the project included a number of components with different requirements (different transaction type, side types, order book uniqueness per symbol, order book records had to be ordered, etc)
- Python OOP workarounds: There were times were I understood why the language is also a "scripting" language, as I had to come up with workarounds for the lack of OOP options (Singleton, maxheap implementation).


## Improvements

- OrderPublisher might be better as a PubSub implementation
- Heap operations can be implemented better (also the logic)
- For now all the commands are implemented in the same OrderCommand. In the future, this should be the interface for each concrete command implementation
- More business logic

## How to run

> pip install -r requirements.txt
> cd src/
> python3 main.py # this will run the program for the attached input_file.csv

Run tests:
> Make sure you are in the src folder.
> ./test/test_scenario.sh

Check permission in case of errors.

## Thank you :)