export PYTHONPATH="$(pwd)"

echo "========================="
echo "==== SCENARIO 1 UNIT ===="
echo "========================="
FOLDER="test/unit"
python3 $FOLDER/OrderBookTest.py > $FOLDER/scenario1.txt
diff -u $FOLDER/scenario1.txt $FOLDER/scenario1_cmp.txt > /dev/null

if [ $? -eq 0 ]; then
    echo "Scenario 1 passed."
else
    echo "Scenario 1 failed."
fi


echo "========================="
echo " SCENARIO 1 INTEGRATION "
echo "========================="
FOLDER="test/integration"
python3 $FOLDER/main.py > $FOLDER/scenario1.txt
diff -u $FOLDER/scenario1.txt $FOLDER/scenario1_cmp.txt > /dev/null

if [ $? -eq 0 ]; then
    echo "Scenario 1 passed."
else
    echo "Scenario 1 failed."
fi