import unittest

from orders.OrderFactory import OrderFactory

class AssertHelper(unittest.TestCase):
    def eq(self, x, expected):
        self.assertEquals(x, expected, f"Value: {x}, expected {expected}")

def csvToOrdersList(filename: str):
    orders = []
    from csv import reader
    with open(filename, 'r') as csvFile:
        csvContent = reader(csvFile)
        for record in csvContent:
            if not record or "#" in record[0] or "N" not in record[0]:
                # we only covered the transaction type N
                continue
            else:
                try:
                    fields = [str(s).strip() for s in record]
                    orders.append(fields)
                except Exception as e:
                    print(e)
    ordersObj = [OrderFactory.createOrder(o) for o in orders]
    return ordersObj