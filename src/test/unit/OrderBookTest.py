from orders.OrderType import OrderType
from orders.models.OrderN import OrderN
from models.SideType import SideType
from processors.ProcessorSymbol import ProcessorSymbol


testProcessor = ProcessorSymbol("TEST")
dummyOrders = [
    OrderN('N', 1, "TEST", 100, 100, 'B', 1),
    OrderN('N', 1, "TEST", 100, 100, 'S', 2),
    OrderN('N', 2, "TEST", 100, 100, 'B', 3),
    OrderN('N', 2, "TEST", 100, 100, 'S', 4),
]

for o in dummyOrders:
    testProcessor.getNotified(o)