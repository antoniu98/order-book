from multiprocessing import Pool, Queue

class MultiThreader:
    def __init__(self) -> None:
        pass

    def init_thread_with_function(self, func):
        q = Queue()
        import threading
        t = threading.Thread(target=self.consumer, args=[func, q])
        t.start()
        t.join()
        return q

    def consumer(self, func, q):
        while True:
            data = q.get()
            func(data)

    def run_function_thread(self, func, *args):
        import threading
        t = threading.Thread(target=func, args=args)
        t.start()
        t.join()