from .ProcessorSymbol import ProcessorSymbol

class ProcessorFactory:
    """
        * Factory that create processor for each symbol. For the moment there is a general ProcessorSymbol. If clients (ex: IBM) needs
        a custom strategy for trading we can create separete processors such as ProcessorIBM. To be discussed
    """
    def createProcessor(symbol: str):
        return ProcessorSymbol(symbol)