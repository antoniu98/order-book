import heapq
from orders.OrderCommand import OrderCommand

class ProcessorSymbol:
    """
        * This class is instantiated for every different symbol, as each one needs an individual order book.
        * Note: Access to order book is only via the api provided by the ProcessorSymbol class
    """

    class OrderBook:
        """
            * OrderBook model with concrete methods for basic book operations: insert, delete, check
            * Using the command pattern, the order book will receive transaction commands
            * Note: As buyOrders are implemented using max-heap, due to python not having a
            concrete implementation for max-heap, we will insert the negative price value of the order to mimic
            max-heap behaviour
        """
        def __init__(self, outterClassInstance) -> None:
            self.outterClassInstance = outterClassInstance
            self.sellOrders = []  # minheap
            self.bestAsk = None   # lowest from sellOrders
            self.buyOrders = []   # maxheap
            self.bestBid = None   # highest from buyOrders

        def pushIntoSellOrders(self, order):
            if not self.bestAsk or self.bestAsk.price <= order.price:
                self.bestAsk = order
            heapq.heappush(self.sellOrders, (+order.price, order))

        def pushIntoBuyOrders(self, order):
            if not self.bestBid or self.bestBid.price >= order.price:
                self.bestBid = order
            heapq.heappush(self.sellOrders, (-order.price, order))

        def popFromSellOrders(self):
            pop = heapq.heappop(self.sellOrders)[1] if len(self.sellOrders) else None
            if len(self.sellOrders) < 1:
                self.bestAsk = None
            else:
                self.bestAsk = heapq.heappop(self.sellOrders)[1]
                self.pushIntoBuyOrders(self.bestAsk)
            return pop

        def popFromBuyOrders(self):
            pop = heapq.heappop(self.buyOrders)[1] if len(self.buyOrders) else None
            if len(self.buyOrders) < 1:
                self.bestBid = None
            else:
                self.bestBid = heapq.heappop(self.buyOrders)[1]
                self.pushIntoBuyOrders(self.bestBid)
            return pop

        def viewSellOrders(self): return self.sellOrders

        def viewBuyOrders(self): return self.buyOrders


    def __init__(self, symbol: str) -> None:
        self.orderBook = self.OrderBook(self)
        self.symbol = symbol

    def getNotified(self, order):
        OrderCommand.execute(order, order.orderType, self)

    def getBestAsk(self):
        return self.orderBook.bestAsk

    def getBestBid(self):
        return self.orderBook.bestBid

    def acceptBestAsk(self):
        self.orderBook.popFromSellOrders()

    def acceptBestBid(self):
        self.orderBook.popFromBuyOrders()

    def addSellOffer(self, order):
        self.orderBook.pushIntoSellOrders(order)

    def addBuyOffer(self, order):
        self.orderBook.pushIntoBuyOrders(order)

