from enum import Enum

class SideType(Enum):
    B = "B"
    S = "S"