from input_output.InputParser import InputParser
from orders.OrderPublisher import OrderPublisher

import os
absolute_path = os.path.dirname(os.path.abspath(__file__))

def main():
    inputParser: InputParser = InputParser()
    orderPublisher: OrderPublisher = OrderPublisher()
    inputParser.run_parallel(absolute_path + "/input_file.csv", orderPublisher)
    
    

if __name__ == "__main__":
    main()