from dataclasses import fields
from orders.OrderPublisher import OrderPublisher
from orders.models.Order import Order
from models.SideType import SideType
from multithreading.MultiThreader import MultiThreader
from orders.OrderFactory import OrderFactory


class InputParser:
    """
        * Parser object will create Order objects from CSV file
    """
    def readFromCSV(csvName: str, orderPublisher: OrderPublisher) -> None:
        orders = []
        from csv import reader
        with open(csvName, 'r') as csvFile:
            csvContent = reader(csvFile)
            for record in csvContent:
                if not record or "#" in record[0] or "N" not in record[0]:
                    # we only covered the transaction type N
                    continue
                else:
                    try:
                        fields = [str(s).strip() for s in record]
                        orders.append(fields)
                    except Exception as e:
                        print(e)
        for o in orders:
            InputParser.fieldsToOrderObject(o, orderPublisher)

    def fieldsToOrderObject(fields, orderPublisher: OrderPublisher):
        order = OrderFactory.createOrder(fields)
        orderPublisher.receiveOrder(order)

    def run_parallel(self, filename: str, orderPublisher: OrderPublisher):
        multiThreader = MultiThreader()
        multiThreader.run_function_thread(InputParser.readFromCSV, filename, orderPublisher)