from multithreading.MultiThreader import MultiThreader

class Logger:
    def __init__(self) -> None:
        self.multiThreader = MultiThreader()

    def print_A(self, order):
        print(f"A {order.user} {order.userOrderId}")

logger = Logger()
