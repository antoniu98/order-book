from dataclasses import dataclass

@dataclass
class Order:
    """
        * Interface for model of an 'order' to be processed for the order book
    """