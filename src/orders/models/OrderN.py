from dataclasses import dataclass
from models.SideType import SideType
from orders.OrderType import OrderType
from orders.models.Order import Order

class OrderN(Order):
    """
        * Order with transactionType N
    """

    orderType: OrderType
    user: int
    symbol: str
    price: int
    qty: int
    side: SideType
    userOrderId: int

    def __init__(self, orderType, user, symbol, price, qty, side, userOrderId):
        Order.__init__(self)
        self.orderType = OrderType[orderType]
        self.user = int(user)
        self.symbol = symbol
        self.price = int(price)
        self.qty = int(qty)
        self.side = SideType[side]
        self.userOrderId = int(userOrderId)

    def __lt__(self, other):
        if (self.price == other.price):
            return self.userOrderId < other.userOrderId
        return self.price < other.price