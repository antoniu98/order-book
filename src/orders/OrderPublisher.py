from processors.ProcessorFactory import ProcessorFactory
from processors.ProcessorSymbol import ProcessorSymbol
from orders.models.Order import Order

class OrderPublisher:
    """
        * Observerer pattern: The OrderPublisher receives orders and redirect them to the assigned processor
        * Future improve: pubsub
    """

    def __init__(self) -> None:
        self.symbolProcessorsMap = {}

    def subscribe(self, processor, symbol: str):
        self.symbolProcessorsMap[symbol] = processor

    def notify(self, order: Order, symbol: str):
        processor = None
        if symbol not in self.symbolProcessorsMap:
            processor = ProcessorFactory.createProcessor(symbol)
            self.subscribe(processor, symbol)
        else:
            processor: ProcessorSymbol = self.symbolProcessorsMap[symbol]
        processor.getNotified(order)

    def receiveOrder(self, order: Order):
        self.notify(order, order.symbol)