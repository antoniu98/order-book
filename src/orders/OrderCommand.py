import orders.models.OrderN as OrderN
import orders.models.Order as Order
import orders.OrderType as ot
import models.SideType as st
import processors.ProcessorSymbol as ProcessorSymbol
from input_output.logger import logger

class OrderCommand:
    """
        * The class provides STATIC methods for processing an order and run business logic depending on its order type (NEW, CANCEL, FLUSH)
        * Future improvement: Use the current class as a Command interface and separate each order command (executeN will be implemented in NewOrderCommand)
    """
    
    @staticmethod
    def execute(order: Order, orderType: ot.OrderType, processor: ProcessorSymbol):
        if orderType is ot.OrderType.N:
            OrderCommand.executeN(order, processor)
        else:
            # implement other executions for C and F orderType
            pass

    @staticmethod
    def executeN(order: OrderN, processor: ProcessorSymbol):
        logger.print_A(order)

        if order.side is st.SideType.B:
            bestAsk = processor.getBestAsk()
            if not bestAsk or bestAsk.price > int(order.price):
                # print(f"Best ask is {'NO_ASK_YET' if not bestAsk else bestAsk.price} and is GREATER than current buy order price {order.price}. Ask refused.")
                processor.addBuyOffer(order)
            else:
                # print(f"Best ask is {bestAsk.price} and is LOWER than current buy order price {order.price}. Ask accepted.")
                processor.acceptBestAsk()

        if order.side is st.SideType.S:
            bestBid = processor.getBestBid()
            if not bestBid or bestBid.price < int(order.price):
                # print(f"Best bid is {'NO_ASK_YET' if not bestBid else bestBid.price} and is LOWER than current sell order price {order.price}. Bid refused.")
                processor.addSellOffer(order)
            else:
                # print(f"Best bid is {bestBid.price} and is GREATER than current sell order price {order.price}. Bid accepted.")
                processor.acceptBestBid()