from enum import Enum

class OrderType(Enum):
    N = "N"
    C = "C"
    F = "F"