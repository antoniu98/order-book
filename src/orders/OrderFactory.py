from .models.OrderN import OrderN

class OrderFactory:
    def createOrder(fields):
        if fields[0] == 'N': return OrderN(*fields)